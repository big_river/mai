# MAI (Mac Apps Installer)

----------
About MAI
----------

mai.sh is a script that quickly gets a fresh install of macOS up and running with a handful of useful GUI and CLI applications. MAI downloads and installs Homebrew, the Cask extension, GUI and CLI apps, as well as creates a .vimrc file.  Please review the code to see which applications are installed.

---------------
OS Requirements
---------------

mai.sh requires macOS 10.14.1; mai.sh is known to fail on 10.14.0.

-----
Notes
-----

1) If Xcode command-line-tools are not installed, Homebrew will install them for you. The session will seem to hang, but it doesn't, it just takes a while to download and install command-line-tools.

2) During the installation, you will be prompted a few times to provide your password to install some of the apps.

3) During the Teamviewer installation, Teamviewer will launch automatically. You can close Teamviewer while the script is running.  About 30 seconds after you close Teamviewer a pop-up will say Teamviewer quit unexpectedly, just click Ignore.

------------
Installation
------------

Note: Do NOT run the script using sudo, run it as a regular user

1) cd mai-master
2) chmod +x mai.sh
3) ./mai.sh 
