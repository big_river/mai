#!/bin/bash
# This script installs Homebrew, the Cask extension, and some helpful GUI and CLI programs on a clean install of macOS.

# -- Functions -- #

# Check if script is ran as root user
function check_sudo() {
    if [ $(id -u) -eq 0 ]; then
        echo ""
        echo "Do not run this script using sudo or as root."
        echo ""
        exit 1
    fi
}

# Logo
function dis_logo() {
cat << EOF

  __    __    ___    _____
 |   \/   |  /   \  |_   _|
 |  _  _  | /  _  \  _| |_
 |__|\/|__|/__/ \__\|_____|

EOF
}

# Create to .vimrc file
function create_vimrc() {
echo ""
echo "Creating .vimrc file..."
cat << EOF > $HOME/.vimrc
" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
" Avoid side effects when it was already reset.
set nocompatible
" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype indent plugin on
" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start
" Show line numbers
set number
" Show syntax highlighting
syntax on
" Tabs are set to 4 spaces && tabs are spaces
set shiftwidth=4
set softtabstop=4
set expandtab
" Highlight current line
set cursorline
" Highlight matching [{()}]
set showmatch
" Better command-line completion
set wildmenu
" Show partial commands in the last line of the screen
set showcmd
" Highlight searches
set hlsearch
" Start searching when typing
set incsearch
" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase
" Display the cursor position on the last line of the screen or in the status
" line of a window
set ruler
" Always display the status line, even if only one window is displayed
set laststatus=2
" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
set confirm
EOF
check_success
}

# Closing message for gdcp setup
function closing_message() {
cat << EOF

Don't forget: run gdcp from the command line
to finish its initial setup. Goodbye!

EOF
}

# Check if command executes correctly
function check_success() {
    if [ $? -eq 0 ]; then
        echo "Success!"
    else
        echo "Failed! Check $HOME/.mai.log"
    fi
}

# Array of apps to be installed via brew
function install_brew_apps() {
    brew_apps=("imagemagick" "vim" "htop" "wget" "nmap" "python@2" "python@3" "telnet" "diffutils" "cowsay")
    for app in "${brew_apps[@]}"; do
        echo ""
        echo "Installing ${app}..."
        brew install "$app" >/dev/null 2>>$HOME/.mai.log
        check_success
    done
}

# Array of apps to be installed via cask
function install_cask_apps() {
    cask_apps=("firefox" "google-chrome" "slack" "sublime-text" "vlc" "gimp" "iterm2" "spotify" "transmit" "flux" "dropbox" "mamp" "discord" "teamviewer" "the-unarchiver" "flycut" "dash" "tunnelblick" "wireshark" "vnc-viewer" "adobe-acrobat-reader")
    for app in "${cask_apps[@]}"; do
        echo ""
        echo "Installing ${app}..."
        brew cask install "$app" >/dev/null 2>>$HOME/.mai.log
        check_success
    done
}

# -- END FUNCTIONS -- #


# -- MAIN -- #

# Ensure the script is not ran using sudo or as root
check_sudo

# Display logo
dis_logo

# Install Homebrew
echo ""
echo "Installing Homebrew..."
sleep 2
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" 2>>$HOME/.mai.log

# Run brew doctor
echo ""
echo "Running brew doctor..."
brew doctor >/dev/null 2>>$HOME/.mai.log
check_success

# Install cask extension
echo ""
echo "Installing cask extension..."
brew tap caskroom/cask >/dev/null 2>>$HOME/.mai.log
check_success

# Install GUI programs via cask extension
install_cask_apps

# Install CLI tools via brew
install_brew_apps


# Install mas for apps found only in Mac App Store
echo ""
echo "Installing mas..."
brew install mas >/dev/null 2>>$HOME/.mai.log
check_success

# mas signin is currently broken
# see the following: https://github.com/mas-cli/mas/issues/164
#
# This code includes a workaround by opening MAS GUI for you to sign in.
# Once signed in, the script continues and the mas_apps are installed.

# If mas is installed, prompt for Apple ID signin
if type mas >/dev/null 2>&1; then
    # Prompt for mas signin
    echo ""
    echo "In order to install applications via mas, you first need to sign in using your Apple ID."
    while true; do
        read -p "Would you like to sign in with your apple ID now [y/n]? " signin
        case "$signin" in
            [yY]* ) open -a /Applications/App\ Store.app
                    read -p "Press RETURN after signing into Mac App Store..." returnkey
                    # Search and install mas apps
                    mas_apps=("Lightshot Screenshot" "Amphetamine" "Parallels Desktop Lite" "Microsoft Remote Desktop 10")
                    for app in "${mas_apps[@]}"; do
                        app_id=$(mas search "$app" | sed -n '1p' | sed 's/^[ \t]*//' | cut -d " " -f 1)
                        echo ""
                        echo "Installing ${app}..."
                        mas install "$app_id" >/dev/null 2>>$HOME/.mai.log
                        check_success
                    done
                    break;;
            [nN]* ) break;;
            * ) echo "Please answer Yes or No...";;
        esac
    done
fi

# Cleanup
echo ""
echo "Cleaning up old files after installs..."
brew cleanup >/dev/null 2>>$HOME/.mai.log
check_success

# Install gdcp dependencies
echo ""
echo "Installing gdcp dependencies via pip..."
pip install pydrive >/dev/null 2>>$HOME/.mai.log
pip install backoff >/dev/null 2>>$HOME/.mai.log
check_success

# Install gdcp
echo ""
echo "Cloning gdcp from GitHub..."
git clone https://github.com/ctberthiaume/gdcp.git >/dev/null 2>>$HOME/.mai.log
check_success

# Copying gdcp to /usr/local/bin/
echo ""
echo "Copying ./gdcp/gdcp to /usr/local/bin..."
cp gdcp/gdcp /usr/local/bin/
check_success

# Delete gdcp dir
echo ""
echo "Deleting cloned gdcp directory (no longer needed)..."
rm -rf gdcp -y
check_success

# Prompt to create .vimrc file
if [ -f $HOME/.vimrc ]; then
    echo ""
    echo "A .vimrc file was found in your home directory"
    while true; do
        read -p "Would you like to OVERWRITE your current .vimrc file with a new .vimrc file [y/n]? " yn
        case $yn in
            [yY]* ) create_vimrc; break;;
            [nN]* ) break;;
            * ) echo "Please choose Yes or No..."
        esac
    done
else
    create_vimrc
fi

# Closing message if gdcp is installed but not setup
if type gdcp >/dev/null 2>&1 && ! [ -f $HOME/.gdcp/client_secrets.json >/dev/null 2>&1 ]; then
    # Use cowsay if available
    if type cowsay >/dev/null 2>&1; then
        closing_message | cowsay
        exit 0
    else
        closing_message
        exit 0
    fi
fi

# -- END MAIN -- #
